var expect = require('expect');

var {generateMessage, generateLocationMessage} = require('./message')

describe('generateMessage', () => {
    it('should generate correct message object', () => {
        var from = 'Jen';
        var text = 'Some message';
        var message = generateMessage(from, text);

        expect(message.createAt).toBeA('number');
        expect(message).toInclude({
            from: from,
            text: text
        })
    })
});

describe('generateLocationMessage', () => {
    it('should generate corrent location object', () => {
        var from = 'Jen';
        var lat = 1;
        var lng = 1;
        var locatioMessage = generateLocationMessage(from, lat, lng);

        expect(locatioMessage.createAt).toBeA('number');
        expect(locatioMessage).toInclude({
            from: from,
            url: `https://www.google.com/maps?q=1,1`
        })
    })
})
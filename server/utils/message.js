var moment = require('moment');



var generateMessage = (from, text) => {
    return {
        from, // = from: from
        text,
        createAt: moment().valueOf()
    }
}

var generateLocationMessage = (from, lat, lng) => {
    return {
        from: from,
        url: `https://www.google.com/maps?q=${lat},${lng}`,
        lat: lat,
        lng: lng,
        createAt: moment().valueOf()
    }
};

module.exports = {generateMessage, generateLocationMessage};
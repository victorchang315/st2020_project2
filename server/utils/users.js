[{
    id: '',
    name: 'Peter',
    room: 'The Office Fans'
}]

// addUser(id, name, room)
// removeUser(id) // 移除side bar 名單
// getUser(id)    // 
// getUserList(room)

class Users {
    constructor () {
        this.users = [];
    }
    addUser (id, name, room) {
        var user = {id, name, room};
        this.users.push(user);
        return user;
    }
    removeUser (id) {
        var user = this.getUser(id);

        if (user) { //if exist
            this.users = this.users.filter((user) => user.id !== id);  // update users array
        }

        return user;
    }
    getUser (id) {
        return this.users.filter((user) => user.id === id)[0];
    }
    getUserList (room) {
        var users = this.users.filter((user) => user.room === room);
        var namesArray = users.map((user) => user.name);

        return namesArray;
    }
}

module.exports = {Users};
var isRealString = (str) => {
    // 驗證是text是字串型態 且去除邊界空白的字串長度>0
    return typeof str === 'string' && str.trim().length > 0; 
}

module.exports = {isRealString}


const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'hi', 100);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'abc', 100);
    await page.keyboard.press('Enter', {delay: 100});
    await page.screenshot({path: 'test/screenshots/welcome.png'});
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.keyboard.press('Enter', {delay: 100});
    await page.screenshot({path: 'test/screenshots/errmsg.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'hi', 100);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'abc', 100);
    await page.keyboard.press('Enter', {delay: 1});

    await page.waitForSelector('#swal2-title');
    let msg = await page.evaluate(() => {
        return document.querySelector('#swal2-title').innerHTML;
    });
    expect(msg).toBe('Welcome!');

    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'abc', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('abc');

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser1 = await puppeteer.launch({ headless: !openBrowser });
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser1.newPage();
    const page2 = await browser2.newPage();
    
    await page1.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100});

    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page2.waitForSelector('#users > ol > li:nth-child(1)');
    await page2.waitForSelector('#users > ol > li:nth-child(2)');  
    let member1 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    let member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    expect(member1).toBe('John');
    expect(member2).toBe('Mike');
    
    await browser1.close();
    await browser2.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    
    await page.waitForSelector('#message-form > button');
    let button = await page.evaluate(() => {
        return document.querySelector('#message-form > button').innerHTML;
    });
    expect(button).toBe('Send');
    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R3', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#message-form > input[type=text]');
    
    
    await page.type('#message-form > input[type=text]', 'hello', 100);
    await page.keyboard.press('Enter', {delay: 100});
    let msg = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });

    expect(msg).toBe('hello');

    await browser.close();
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser1 = await puppeteer.launch({ headless: !openBrowser });
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser1.newPage();
    const page2 = await browser2.newPage();

    await page1.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100});
    await page1.waitForSelector('#message-form > input[type=text]');
    await page1.type('#message-form > input[type=text]', 'Hi', 100);
    await page1.keyboard.press('Enter', {delay: 100});

    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});
    await page2.waitForSelector('#message-form > input[type=text]');
    await page2.type('#message-form > input[type=text]', 'Hello', 100);
    await page2.keyboard.press('Enter', {delay: 100});
    await page1.waitForSelector('#messages> li:nth-child(3)');
    await page1.waitForSelector('#messages> li:nth-child(4)');
    let msg1 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });

    let msg2 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(2) > p').innerHTML;
    });

    expect(msg1).toBe('Hi');
    expect(msg2).toBe('Hello');

    await browser1.close();
    await browser2.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R4', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#send-location');
    let button = await page.evaluate(() => {
        return document.querySelector('#send-location').innerHTML;
    });
    expect(button).toBe('Send location');
    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R11', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#send-location');
    await page.click('#send-location');
    await page.waitForSelector('#mapDiv');
    let mapMsg = await page.evaluate(() => {
        return document.querySelector('#mapDiv');
    });
    expect(mapMsg).not.toBe(null);
    await browser.close();
})
